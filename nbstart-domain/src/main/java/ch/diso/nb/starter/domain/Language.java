package ch.diso.nb.starter.domain;

public enum Language {
    GERMAN("de"), FRENCH("fr"), ITALIAN("it"), ENGLISH("en");

    private final String cultureTag;

    Language(String cultureTag) {
        this.cultureTag = cultureTag;
    }

    public static Language fromValue(String language) {
        for (Language value : Language.values()) {
            if (value.cultureTag.equalsIgnoreCase(language)) {
                return value;
            }
        }
        throw new IllegalArgumentException(String.format("Unknown language: {}", language));
    }

    public String getCultureTag() {
        return cultureTag;
    }
}
