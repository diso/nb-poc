package ch.diso.nb.starter.domain;

public enum WorkstationType {
    PUBLIC, PROTECTED, MULTIMEDIA;
}
