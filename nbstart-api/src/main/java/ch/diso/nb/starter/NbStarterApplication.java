package ch.diso.nb.starter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/nb/starter")
public class NbStarterApplication extends Application {
}
