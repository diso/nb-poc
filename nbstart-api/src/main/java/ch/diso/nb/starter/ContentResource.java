package ch.diso.nb.starter;

import ch.diso.nb.starter.application.CmsAdapter;
import ch.diso.nb.starter.cms.CmsAdapterImpl;
import ch.diso.nb.starter.domain.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("content")
@RequestScoped
public class ContentResource {

    @Inject
    private CmsAdapter cmsAdapter;

    @GET
    @Path("{language}/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContent(@PathParam("language") String language, @PathParam("key") String key) {
        try {
            return Response.ok(cmsAdapter.getWebContent(Language.fromValue(language), key)).build();
        } catch (WebContentNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/angebote/{language}/{workstationType}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkstationAngebot(@PathParam("language") String language, @PathParam("workstationType") String workstationType) {
        try {
            return Response.ok(cmsAdapter.getWorkstationAngebot(Language.fromValue(language), WorkstationType.valueOf(workstationType.toUpperCase()))).build();
        } catch (WebContentNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @DELETE
    @Path("{language}/{key}")
    public Response deleteContent(@PathParam("language") String language, @PathParam("key") String key) {
        try {
            cmsAdapter.deleteWebContent(Language.fromValue(language), key);
            return Response.ok().build();

        } catch (WebContentNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateContent(WebContent webContent) {
        try {
            return Response.ok(cmsAdapter.updateWebContent(webContent)).build();
        } catch (WebContentNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addContent(WebContent webContent) {
        try {
            return Response.ok(cmsAdapter.createWebContent(webContent)).build();
        } catch (WebConentAlreadyExistsException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
