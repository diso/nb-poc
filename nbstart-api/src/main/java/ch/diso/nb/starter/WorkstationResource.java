package ch.diso.nb.starter;

import ch.diso.nb.starter.application.CmsAdapter;
import ch.diso.nb.starter.domain.Language;
import ch.diso.nb.starter.domain.WebContentNotFoundException;
import ch.diso.nb.starter.domain.WorkstationAngebot;
import ch.diso.nb.starter.domain.WorkstationType;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("workstation")
@RequestScoped
public class WorkstationResource {

    @GET
    public Response getWorkstationType() {
        return Response.ok(WorkstationType.valueOf(System.getProperty("arbeitsstationtyp","public").toUpperCase())).build();
    }

    @POST
    @Path("logoff")
    public Response executeLogoff() {
        try {
            String logoffCommand;
            String operatingSystem = System.getProperty("os.name");
            if ("Linux".equals(operatingSystem) || "Mac OS X".equals(operatingSystem)) {
                logoffCommand = "launchctl bootout user/ben";
            } else if (operatingSystem.contains("indows")) {
                logoffCommand = "logoff";
            } else {
                throw new RuntimeException(String.format("Unsupported operating system. %s", operatingSystem));
            }
            Runtime.getRuntime().exec(logoffCommand);
            return Response.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @POST
    @Path("execute/{angebotId}")
    public Response executeAngebot(@PathParam("angebotId") String angebotId) {
        try {
            String command = findCommandByAngebotId(angebotId);
            Runtime runtime = Runtime.getRuntime();
            runtime.exec(command);
            return Response.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().build();
        }

    }

    private String findCommandByAngebotId(String angebotId) {
        String lang = angebotId.split("-")[0];
        String angebot = angebotId.split("-")[1];
        return COMMANDS.get(angebot);
    }

    private static final Map<String, String> COMMANDS = new HashMap<>();
    static {
        if (System.getProperty("os.name").toLowerCase().indexOf("indows") > -1) {
            COMMANDS.put("Firefox","C:\\Program Files\\Mozilla Firefox\\firefox.exe -new-window");
            COMMANDS.put("Chrome","C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe");
            COMMANDS.put("DisoCollection","C:\\Program Files\\Mozilla Firefox\\firefox.exe -new-window http://www.diso.ch");
        } else {
            COMMANDS.put("Firefox", "/Applications/Firefox.app/Contents/MacOS/firefox -new-window");
            COMMANDS.put("Chrome", "/Applications/Chrome.app/Contents/MacOS/Chrome");
            COMMANDS.put("DisoCollection","/Applications/Firefox.app/Contents/MacOS/firefox  -new-window http://www.diso.ch");
        }
    }

}
