package ch.diso.nb.starter.application;

import ch.diso.nb.starter.domain.*;

import java.util.List;

public interface CmsAdapter {

    WebContent createWebContent(WebContent webContent) throws WebConentAlreadyExistsException;

    WebContent updateWebContent(WebContent webContent) throws WebContentNotFoundException;

    void deleteWebContent(Language language, String key) throws WebContentNotFoundException;

    WebContent getWebContent(Language language, String key) throws WebContentNotFoundException;

    List<WorkstationAngebot> getWorkstationAngebot(Language language, WorkstationType workstationType) throws WebContentNotFoundException;
}
