package ch.diso.nb.starter.cms;

import ch.diso.nb.starter.application.CmsAdapter;
import ch.diso.nb.starter.domain.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

@Dependent
public class CmsAdapterImpl implements CmsAdapter {

    @Override
    public WebContent createWebContent(WebContent webContent) throws WebConentAlreadyExistsException {
        return null;
    }

    @Override
    public WebContent updateWebContent(WebContent webContent) throws WebContentNotFoundException {
        return null;
    }

    @Override
    public void deleteWebContent(Language language, String key) throws WebContentNotFoundException {

    }

    @Override
    public WebContent getWebContent(Language language, String key) throws WebContentNotFoundException {
        try (InputStream inputStream = getClass().getResourceAsStream(String.format("/cms/%s.json", key))) {
            try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8)) {
                JsonObject jsonObject = new Gson().fromJson(inputStreamReader, JsonObject.class);
                WebContent webContent = new WebContent();
                webContent.setLanguage(language.getCultureTag());
                webContent.setKey(key);
                webContent.setContent(readTranslatedValue(jsonObject.get("content").getAsJsonArray(), language));
                return webContent;
            }
        } catch (IOException e) {
            throw new WebContentNotFoundException();
        }
    }

    @Override
    public List<WorkstationAngebot> getWorkstationAngebot(Language language, WorkstationType workstationType) throws WebContentNotFoundException {
        try (InputStream inputStream = getClass().getResourceAsStream(String.format("/cms/angebote-%s.json", workstationType.name().toLowerCase()))) {
            try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF_8)) {
                JsonObject jsonObject = new Gson().fromJson(inputStreamReader, JsonObject.class);
                JsonArray jsonArray = jsonObject.getAsJsonArray("angebote");
                List<WorkstationAngebot> workstationAngebotList = new ArrayList<>();
                for (JsonElement jsonElement : jsonArray) {
                    JsonObject angebotJson = jsonElement.getAsJsonObject();
                    WorkstationAngebot workstationAngebot = new WorkstationAngebot();
                    workstationAngebot.setIco(angebotJson.get("ico").getAsString());
                    workstationAngebot.setName(readTranslatedValue(angebotJson.get("name").getAsJsonArray(), language));
                    workstationAngebot.setDescription(readTranslatedValue(angebotJson.get("description").getAsJsonArray(), language));
                    workstationAngebotList.add(workstationAngebot);
                }
                return workstationAngebotList;
            }
        } catch (IOException e) {
            throw new WebContentNotFoundException();
        }
    }

    private String readTranslatedValue(JsonArray jsonArray, Language language) {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            if (jsonObject.keySet().contains(language.getCultureTag())) {
                return jsonObject.get(language.getCultureTag()).getAsString();
            }
        }
        throw new IllegalArgumentException(String.format("Content is missing a translation for language %s", language));
    }
}
